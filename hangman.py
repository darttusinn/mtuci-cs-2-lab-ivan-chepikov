# Problem Set 2, hangman.py
# Hangman Game
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    for i in secret_word:

      if i not in letters_guessed:
        return False
        
    return True



def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    res = ''

    for i in secret_word:

        if i in letters_guessed:
            res += i

        else:
            res += ' _ '   

    return res



def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    res = ''

    for letter in string.ascii_lowercase:

      if letter not in letters_guessed:
        res += letter

    return res



    

'''def hangman(secret_word, letters_guessed = []):

    guesses = 6
    warnings = 3

    while True:

      print('I thinking you have ', guesses, ' guesses!!!')
      print('Also you have ', warnings, ' warnings!!!, please use alphabet')
      print('Available letters ', get_available_letters(letters_guessed))
      
      letter = input('Enter letter: ')
      if letter not in string.ascii_lowercase or letter in letters_guessed:
        warnings -= 1

        if letter in letters_guessed:
          print('You have already guessed that letter, warnings left - ', warnings, get_guessed_word(secret_word,letters_guessed))

          if warnings == 0:
            print('You lose. ')
            print('Word was - ', secret_word)
            print('------------------')
            break

        else:
          print("It's invalid letter, warnings left - ", warnings, get_guessed_word(secret_word,letters_guessed))

          if warnings == 0:
            print('You lose. ')
            print('Word was - ', secret_word)
            print('------------------')
            break

      else:
        letters_guessed.append(letter)

        if is_word_guessed(secret_word,letters_guessed) == True:

          print('You won!!!')
          print('Word was - ', secret_word)
          print('Your score for this game ', guesses * len(letters_guessed))
          print('------------------')

          break

        else:
          if letter not in secret_word:
            print("It's not my word ", get_guessed_word(secret_word,letters_guessed))
            guesses -= 1

          else:
            print("Good guess ", get_guessed_word(secret_word,letters_guessed))

      if guesses == 0:

        print('You lose.')
        print('Word was - ', secret_word)
        print('------------------')

        break

      print('------------------')'''



def match_with_gaps(my_word, other_word):

  my_word = my_word.replace(' _ ','_')

  if my_word.count('_') == 1:
    return False

  else:
    if len(my_word.strip()) == len(other_word):

      for i in range(len(other_word)):

        if my_word[i] != '_' and other_word[i] not in my_word[i]:
          return False

        else:
          continue

      return True

    else:
      return False



def show_possible_matches(my_word):

  flag = False

  for word_from_wordlist in wordlist:

    if match_with_gaps(my_word, word_from_wordlist):
      flag = True
      print(word_from_wordlist, end = ' ')
  
  if flag == False:
    print('No matches found')
  
  else:
    return ''



def hangman_with_hints(secret_word, letters_guessed = []):

    guesses = 6
    warnings = 3

    while True:

      print('I am thinking you have ', guesses, ' guesses!!!')
      print('Also you have ', warnings, ' warnings!!!, please use alphabet')
      print('Available letters ', get_available_letters(letters_guessed))
      
      letter = input('Enter letter: ')
      if letter == '*':
        print('------------------')
        print('Maybe this')
        print(show_possible_matches(get_guessed_word(secret_word,letters_guessed)))
        print('------------------')

      else:

        if letter not in string.ascii_lowercase or letter in letters_guessed:
          warnings -= 1

          if letter in letters_guessed:
            print('You have already guessed that letter, warnings left - ', warnings, get_guessed_word(secret_word,letters_guessed))

            if warnings == 0:
              print('You lose. ')
              print('Word was - ', secret_word)
              print('------------------')
              break

          else:
            print("It's invalid letter, warnings left - ", warnings, get_guessed_word(secret_word,letters_guessed))

            if warnings == 0:
              print('You lose. ')
              print('Word was - ', secret_word)
              print('------------------')
              break

        else:
          letters_guessed.append(letter)

          if is_word_guessed(secret_word,letters_guessed) == True:

            print('You won!!!')
            print('Word was - ', secret_word)
            print('Your score for this game ', guesses * len(letters_guessed))
            print('------------------')

            break

          else:
            if letter not in secret_word:
              print("It's not my word ", get_guessed_word(secret_word,letters_guessed))
              guesses -= 1

            else:
              print("Good guess ", get_guessed_word(secret_word,letters_guessed))

        if guesses == 0:

          print('You lose.')
          print('Word was - ', secret_word)
          print('------------------')

          break

        print('------------------')      



if __name__ == "__main__":
    
    print('------------------')
    print('Welcome to the game!!!')
    secret_word = choose_word(wordlist)
    print('I am thinking of a word that is ',  len(secret_word),  ' letters long.')
    print('------------------')
    hangman_with_hints(secret_word)
